{
  inputs = {
    nixpkgs.url = "nixpkgs";
    devshell.url = github:numtide/devshell;
  };

  outputs = { devshell, ... } @ inputs: let
    nixpkgs = inputs.nixpkgs.lib.recursiveUpdate
      inputs.nixpkgs
      (import inputs.nixpkgs {
        system = "x86_64-linux";
        overlays = [ devshell.overlay ];
      })
    ;
  in {
    devShell.x86_64-linux = nixpkgs.pkgs.devshell.fromTOML ./devshell.toml;
  };
}
